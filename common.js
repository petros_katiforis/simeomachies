// The common.js file holds some shared behaviour between game modes to update the clinet.
// The game can be either running on multiplayer or singleplayer mode, but this file
// is completely independent because it relies on just a simple state object.

// Fetching some important elements
const options_container = document.getElementById("options");
const flag_img = document.getElementById("flag");

const flag_codes = ["ad", "ae", "af", "ag", "ai", "al", "am", "ao", "aq", "ar",
                    "as", "at", "au", "aw", "ax", "az", "ba", "bb", "bd", "be",
                    "bf", "bg", "bh", "bi", "bj", "bl", "bm", "bn", "bo", "bq",
                    "br", "bs", "bt", "bv", "bw", "by", "bz", "ca", "cc", "cd",
                    "cf", "cg", "ch", "ci", "ck", "cl", "cm", "cn", "co", "cr",
                    "cu", "cv", "cw", "cx", "cy", "cz", "de", "dj", "dk", "dm",
                    "do", "dz", "ec", "ee", "eg", "eh", "er", "es", "et", "fi",
                    "fj", "fk", "fm", "fo", "fr", "ga", "gb", "gb-eng", "gb-nir", "gb-sct",
                    "gb-wls", "gd", "ge", "gf", "gg", "gh", "gi", "gl", "gm", "gn",
                    "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy", "hk", "hn",
                    "hr", "ht", "hu", "id", "ie", "il", "im", "in", "io", "iq",
                    "ir", "is", "it", "je", "jm", "jo", "jp", "ke", "kg", "kh",
                    "ki", "km", "kn", "kp", "kr", "kw", "ky", "kz", "la", "lb",
                    "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", "ly", "ma",
                    "mc", "md", "me", "mg", "mh", "mk", "ml", "mm", "mn", "mo",
                    "mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my",
                    "mz", "na", "nc", "ne", "nf", "ng", "ni", "nl", "no", "np",
                    "nr", "nu", "nz", "om", "pa", "pe", "pf", "pg", "ph", "pk",
                    "pl", "pm", "pn", "pr", "ps", "pt", "pw", "py", "qa", "re",
                    "ro", "rs", "ru", "rw", "sa", "sb", "sc", "sd", "se", "sg",
                    "sh", "si", "sk", "sl", "sm", "sn", "so", "sr", "ss", "st",
                    "sv", "sx", "sy", "sz", "tc", "td", "tf", "tg", "th", "tj",
                    "tk", "tl", "tm", "tn", "to", "tr", "tt", "tv", "tw", "tz",
                    "ua", "ug", "us", "uy", "uz", "va", "vc", "ve", "vg", "vi",
                    "vn", "vu", "wf", "ws", "xk", "ye", "yt", "za", "zm", "zw"];

const flag_names = ["Ανδόρρα", "Ηνωμένα Αραβικά Εμιράτα", "Αφγανιστάν", "Αντίγκουα και Μπαρμπύντα",
		    "Αγκουίλα", "Αλβανία", "Αρμενία", "Ανγκόλα", "Ανταρκτική",
		    "Αργεντινή", "Αμερικανική Σαμόα", "Αυστρία", "Αυστραλία", "Αρούμπα",
		    "Νήσοι Όλαντ", "Αζερμπαϊτζάν", "Βοσνία και Ερζεγοβίνα", "Μπαρμπάντος",
		    "Μπανγκλαντές", "Βέλγιο", "Μπουρκίνα Φάσο", "Βουλγαρία", "Μπαχρέιν",
		    "Μπορούντι", "Μπενίν", "Αγίος Βαρθολομαίος", "Βερμούδες", "Μπρουνέι",
		    "Βολιβία", "Μποναίρ", "Βραζιλία", "Μπαχάμες", "Μπουτάν",
		    "Νησί Μπουβέ", "Μποτσουάνα", "Λευκορωσία", "Μπελίζ", "Καναδάς",
		    "Νήσοι Κόκος", "Λαϊκη Δημοκρατία του Κονγκό", "Κεντροαφρικανική Δημοκρατία", "Δημοκρατία του Κονγκό",
		    "Ελβετία", "Ακτή Ελεφαντοστού", "Νήσοι Κουκ", "Χιλή",
		    "Καμερούν", "Κίνα", "Κολομβία", "Κόστα Ρίκα", "Κούβα",
		    "Πράσινο Ακρωτήριο", "Κουρασάο", "Νησί των Χριστουγέννων", "Κύπρος", "Τσεχία",
		    "Γερμανία", "Ντζιμπουτί", "Δανία", "Δομινίκα", "Άγιος Δομίνικος",
		    "Αλγερία", "Ισημερινός", "Εσθονία", "Αίγυπτος", "Δυτική Σαχάρα",
		    "Ερυθραία", "Ισπανία", "Αιθιοπία", "Φινλανδία", "Φίτζι",
		    "Νήσοι Φώκλαντ", "Μικρονησία", "Νησία Φερόε", "Γαλλία", "Γκαμπόν",
		    "Ηνωμένο Βασίλειο", "Αγγλία", "Βόρεια Ιρλανδία", "Σκωτία", "Ουαλία",
		    "Γρενάδα", "Γεωργία", "Γαλλική Γουιάνα", "Γκέρνζι", "Γκάνα",
		    "Γιβραλτάρ", "Γρoιλανδία", "Γκάμπια", "Γουινέα",
		    "Γουαδελούπη", "Ισημερινή Γουινέα", "Ελλάδα", "Νότιος Γεωργία και Νότις Σάντουιτς Νήσοι",
		    "Γουατεμάλα", "Γκουάμ", "Γοινέα Μπισσάου", "Γουιάνα", "Χονκ Κονγκ",
		    "Ονδούρας", "Κροατία", "Αϊτί", "Ουγγαρία", "Ινδονησία", "Ιρλανδία",
		    "Ισραήλ", "Νήσος Μαν", "Ινδία", "Βρετανικό Έδαφος Ινδικού Ωκεανού", "Ιράκ", "Ιράν",
		    "Ισλανδία", "Ιταλία", "Τζέρσεϋ", "Τζαμάικα", "Ιορδανία", "Ιαπωνία",
		    "Κένυα", "Κιργιζία", "Καμπότζη", "Κιριμπάτι", "Κομόρες",
		    "Άγιος Χριστόφορος και Νέβις", "Βόρεια Κορέα", "Νότια Κορέα", "Κουβέιτ", "Νήσοι Κέιμαν",
		    "Καζακστάν", "Λάος", "Λίβανος", "Νήσος Αγίας Ελένης", "Λιχτενστάιν",
		    "Σρι Λάνκα", "Λιβερία", "Λεσόθο", "Λιθουανία", "Λουξεμβούργο",
		    "Λετονία", "Λιβύη", "Μαρόκο", "Μονακό", "Μολδαβία", "Μαυροβούνιο",
		    "Μαδαγασκάρη", "Νήσοι Μάρσαλ", "Μακεδονία", "Μάλι", "Μιανμάρ",
		    "Μογκολία", "Μακάου", "Βόρειες Μαριάνες Νήσοι", "Μαρτινίκα", "Μαυριτανία", "Μονστερράτ",
		    "Μάλτα", "Μαυρίκιος", "Μαλδίβες", "Μαλάουι", "Μεξικό", "Μαλαισία",
		    "Μοζαμβίκη", "Ναμίμπια", "Νέα Καληδονία", "Νίγηρας", "Νήσοι Νόρφολκ",
		    "Νιγηρία", "Νικαράγουα", "Ολλανδία", "Νορβηγία", "Νεπάλ", "Ναουρού",
		    "Νιούε", "Νέα Ζηλανδία", "Ομάν", "Παναμάς", "Περού", "Γαλλική Πολυνησία",
		    "Παπούα Νέα Γουινέα", "Φιλιππίνες", "Πακιστάν", "Πολωνία", "Σαιν Πιερ και Μικελόν",
		    "Νήσοι Πίτκαιρν", "Πουέρτο Ρίκο", "Παλαιστίνη", "Πορτογαλία", "Παλάου",
		    "Παραγουάη", "Κατάρ", "Ρεγιουνιόν", "Ρουμανία", "Σερβία", "Ρωσία",
		    "Ρουάντα", "Σαουδική Αραβία", "Νήσοι Σολομώντα", "Σεϋχέλλες", "Σουδάν",
		    "Σουηδία", "Σιγκαπούρη", "Αγία Ελένη, Ασενσιόν και Τριστάν ντα Κούνια", "Σλοβενία",
		    "Σλοβακία", "Σιέρρα Λεόνε", "Αγιος Μαρίνος", "Σενεγάλη", "Σομαλία",
		    "Σουρινάμ", "Νότιο Σουδάν", "Σάο Τομέ και Πρίνσιπε", "Ελ Σαλβαδόρ", "Αγίος Μαρτίνος",
		    "Συρία", "Σουαζιλάνδη", "Νήσοι Τερκς", "Τσαντ", "Γαλλικά Νότια και Ανταρκτικά Εδάφη", "Τόγκο",
		    "Ταϊλάνδη", "Ταζικιστάν", "Τοκελάου", "Ανατολικό Τιμόρ", "Τουρκμενιστάν",
		    "Τυνησία", "Βασίλειο των Τόνγκα", "Τουρκία", "Τρινιντάντ και Τομπάγκο", "Τουβαλού", "Ταϊβάν",
		    "Τανζανία", "Ουκρανία", "Ουγκάντα", "ΗΠΑ", "Ουρουγουάη", "Ουζμπεκιστάν",
		    "Βατικανό", "Άγιος Βικέντιος", "Βενεζουέλα", "Βρετανικές Παρθένοι Νήσοι", "Αμερικανικές Παρθένοι Νήσοι",
		    "Βιετνάμ", "Βανουάτου", "Νήσοι Ουάλις και Φοτούνα", "Σαμόα", "Κόσοβο", "Yεμένη",
		    "Μαγιότ", "Νότια Αφρική", "Ζάμπια", "Ζιμπάμπουε"];

// Some simple configuration
const TOTAL_OPTIONS = 8;
const SHOW_RESULTS_DURATION = 1500;

// Sound effects
const correct_sfx = new Audio("correct.wav");
const wrong_sfx = new Audio("wrong.wav");

let can_submit_answer = true;

function element_play_animation(element, animation) {
    element.classList.remove(animation);

    // This line forces a re-render
    void element.offsetWidth;
    
    element.classList.add(animation);
}

function generate_buttons() {
    for (let i = 0; i < TOTAL_OPTIONS; i++) {
        let button = document.createElement("button");
        options_container.appendChild(button);
    }
}

// Fill the state with some random values to form the next question
function randomly_generate_state(state) {
    state.question_index = Math.floor(Math.random() * flag_codes.length);
    state.country_options.length = 0;
    
    // Generating the answers
    while (state.country_options.length < TOTAL_OPTIONS) {
        let random_index = Math.floor(Math.random() * flag_codes.length);

        // Skip if the guess was unlucky
        if (random_index == state.question_index || flag_names[random_index] in state.country_options) continue;

        state.country_options.push(flag_names[random_index]);
    }

    // Place the correct answer among the options
    state.answer_index = Math.floor(Math.random() * TOTAL_OPTIONS);
    state.country_options[state.answer_index] = flag_names[state.question_index];
}

function client_show_results(state, is_client_winner) {
    let correct_button = options_container.children[state.answer_index];
    correct_button.classList = is_client_winner ? "correct" : "wrong";

    // Playing a sound effect for better feedback
    if (is_client_winner) {
        correct_sfx.play();

    } else {
        wrong_sfx.play();
    }
    
    setTimeout(() => {
        correct_button.classList = "";
        can_submit_answer = true;
    }, SHOW_RESULTS_DURATION);
}

function update_client_based_on_state(state) {
    flag_img.src = `flags/${flag_codes[state.question_index]}.png`;

    for (let i = 0; i < TOTAL_OPTIONS; i++) {
        options_container.children[i].innerText = state.country_options[i];
    }

    element_play_animation(flag_img, "smooth-scale");
}
