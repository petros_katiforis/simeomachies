const id_input = document.getElementById("id_input");
const connect_button = document.getElementById("connect_button");
const score_element = document.getElementById("score");

// This user now acts as a client and connects to another peer
// who has navigated to /server and thus has started a server

function update_score_interface(state) {
    score_element.innerText = `Εσύ: ${state.scores.client}, Φίλος: ${state.scores.server}`;   
}

connect_button.addEventListener("click", () => {
    let conn = peer.connect(id_input.value);

    conn.on("open", () => {
        console.log("I connected to the server");
    });

    conn.on("close", announce_friend_disconnection);
    conn.on("disconnect", announce_friend_disconnection);

    // Handling chat events
    chat_form.addEventListener("submit", (event) => {
        event.preventDefault();
        if (!is_chat_input_valid()) return;

        conn.send({
            "type": "CLIENT_SENT_MESSAGE",
            "content": chat_input.value
        });
    });

    conn.on("data", function (event) {
        // Handling events that are received from the "server" peer
        switch (event.type) {
        case "START_GAME":
            multiplayer_prepare_interface();
            can_submit_answer = true;
            update_client_based_on_state(event.state);

            // Sending answer events when the options are being clicked
            for (let i = 0; i < TOTAL_OPTIONS; i++) {
                options_container.children[i].addEventListener("click", () => {
                    if (!can_submit_answer) return;
                    can_submit_answer = false;
                    
                    conn.send({
                        "type": "CLIENT_ANSWERED",
                        "answer": i
                    });
                });
            }

            break;

        case "UPDATE_CHAT":
            update_chat_interface(event.state);
            
            break;
            
        case "CLIENT_ANSWER_PROCESSED":
            client_show_results(event.state, event.was_client_correct);
            update_score_interface(event.state);

            break;
            
        case "UPDATE_STATE":
            can_submit_answer = true;
            update_client_based_on_state(event.state);

            break;

        case "SERVER_ANSWERED":
            can_submit_answer = false;
            client_show_results(event.state, !event.was_server_correct);
            update_score_interface(event.state);
            
            break;
        }
    });
});
