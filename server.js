const id_element = document.getElementById("id_element");
const score_element = document.getElementById("score");

// The peer to peer "server" node will also hold all the game state
// which will be accessed through game.js, plus some additional variables
let state = {
    "is_multiplayer": true,
    "question_index": null,
    "answer_index": null,
    "country_options": [],
    "scores": {},
    "chat_history": []
}

function generate_new_question(conn) {
    randomly_generate_state(state);
    can_submit_answer = true;
    
    conn.send({
        "type": "UPDATE_STATE",
        "state": state
    });

    update_client_based_on_state(state);
}

function increment_score_of_winner(was_server_correct) {
    let winner = was_server_correct ? "server" : "client";

    state.scores[winner] += 1;
    score_element.innerText = `Εσύ: ${state.scores.server}, Φίλος: ${state.scores.client}`;
}

function add_to_chat(sender_id, content) {
    state.chat_history.unshift({
        "sender_id": sender_id,
        "content": content
    });

    state.chat_history.length = MAX_CHAT_MESSAGES;
}

peer.on("open", () => {
    id_element.innerHTML = `Το προσωπικό σου ID: <b>${peer.id}</b>`;

    // Waiting for client connection
    peer.on("connection", function (conn) {
        console.log("A client has been connected, the game can start!");
        multiplayer_prepare_interface();
        
        randomly_generate_state(state);
        state.scores.client = state.scores.server = 0;
        
        // Send the initial, generated state once the connection has been truly established
        conn.on("open", () => {
            conn.send({
                "type": "START_GAME",
                "state": state
            });
        });

        update_client_based_on_state(state);

        // Adding events to option button in accordance with the server's design
        for (let i = 0; i < TOTAL_OPTIONS; i++) {
            options_container.children[i].addEventListener("click", () => {
                if (!can_submit_answer) return;

                can_submit_answer = false;
                
                const is_correct = (i === state.answer_index);
                increment_score_of_winner(is_correct);
                
                conn.send({
                    "type": "SERVER_ANSWERED",
                    "was_server_correct": is_correct,
                    "state": state
                });

                client_show_results(state, is_correct);

                setTimeout(() => {
                    generate_new_question(conn);
                }, SHOW_RESULTS_DURATION);
            });
        }

        // Registering some client-sent events
        conn.on("data", function (event) {
            // Check if an answer event was received from the other peer
            switch (event.type) {
            case "CLIENT_ANSWERED":
                // If the server has already answered and is waiting the results just ignore this request
                // Does this mean that the server has a natural advantage because of no latency?
                if (!can_submit_answer) break;
                
                const was_client_correct = (event.answer === state.answer_index);
                can_submit_answer = false;
                
                increment_score_of_winner(!was_client_correct);
                client_show_results(state, !was_client_correct);

                conn.send({
                    "type": "CLIENT_ANSWER_PROCESSED",
                    "was_client_correct": was_client_correct,
                    "state": state
                });
                
                setTimeout(() => {
                    generate_new_question(conn);
                }, SHOW_RESULTS_DURATION);

                break;

            case "CLIENT_SENT_MESSAGE":
                add_to_chat(conn.peer, event.content);
                update_chat_interface(state);

                conn.send({
                    "type": "UPDATE_CHAT",
                    "state": state
                });
                
                break;
            }
        });

        // Handling client disconnection
        conn.on("close", announce_friend_disconnection);
        conn.on("disconnect", announce_friend_disconnection);

        // Handling chat messaging
        chat_form.addEventListener("submit", (event) => {
            event.preventDefault();
            if (!is_chat_input_valid()) return;

            add_to_chat(peer.id, chat_input.value);
            
            conn.send({
                "type": "UPDATE_CHAT",
                "state": state
            });

            update_chat_interface(state);
        });
    });
});
