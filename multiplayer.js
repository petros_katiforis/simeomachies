// This file contains some common for all multiplayer-related scripts (that is connect.js and server.js)

const game_container = document.getElementById("game");
const lobby_container = document.getElementById("lobby");
const chat_container = document.getElementById("chat");
const chat_form = document.getElementById("chat-form");
const chat_input = document.getElementById("chat-input");
const chat_empty = document.getElementById("chat-empty");

const MAX_CHAT_MESSAGES = 5;
const MAX_MESSAGE_LENGTH = 180;

// When the page has loaded, the game should be initially hidden
game_container.style["display"] = "none";
let peer = new Peer();

function multiplayer_prepare_interface() {
    generate_buttons();
    game_container.style["display"] = "block";
    lobby_container.style["display"] = "none";
}

function create_chat_text(chat_message) {
    let prefix = chat_message.sender_id === peer.id ? "Εσύ" : "Φίλος";

    return `[${prefix}]: ${chat_message.content}`;
}

function announce_friend_disconnection() {
    alert("Ο φίλος σου αποσυνδέθηκε");
    window.location.reload();
}

// In the future, I could add other conditions to implement moderation and filtering
// Although this is currently no in my plans, the two players are nevertheless friends
function is_chat_input_valid() {
    return (chat_input.value.length != 0 && chat_input.value.length <= MAX_MESSAGE_LENGTH);
}

function update_chat_interface(state) {
    // Remove the empty chat message if messages are present
    if (state.chat_history.length > 0) {
        chat_empty.style["display"] = "none";
    }
    
    // If the current user sent the last message, clear his input
    if (state.chat_history[0].sender_id === peer.id) {
        chat_input.value = "";
    }
    
    // The first element is the one that was created
    if (chat_container.children.length < MAX_CHAT_MESSAGES) {
        let new_message = document.createElement("p");

        new_message.innerText = create_chat_text(state.chat_history[0]);
        chat_container.appendChild(new_message);

    } else {
        for (let i = 0; i < MAX_CHAT_MESSAGES; i++) {
            chat_container.children[MAX_CHAT_MESSAGES - 1 - i].innerHTML = create_chat_text(state.chat_history[i]);
	}
    }
}
