// Some extra code to make single player work
// This file depends on common.js which contains useful code snippets regardless of game

const combo_element = document.getElementById("combo");

let state = {
    // In single player mode, the score will be replaced by a combo indication
    // It's more fun and useful when practising
    "combo": 0,
    
    "answer_index": null,
    "question_index": null,
    "country_options": [],
};

// First, we generate the initial state and the buttons
generate_buttons();
randomly_generate_state(state);
update_client_based_on_state(state);

for (let i = 0; i < TOTAL_OPTIONS; i++) {
    options_container.children[i].addEventListener("click", () => {
        if (!can_submit_answer) return;

        can_submit_answer = false;
        let is_correct = i === state.answer_index;

        state.combo = is_correct ? state.combo + 1 : 0;

        // Unpdating the score UI
        combo_element.innerText = `Σερί: ${state.combo}`;
        client_show_results(state, is_correct);

        setTimeout(() => {
            randomly_generate_state(state);
            update_client_based_on_state(state);
        }, SHOW_RESULTS_DURATION);
    });
}
